CREATE TABLE profile_layout(
    profile INTEGER NOT NULL,
    widget INTEGER NOT NULL,
    position INTEGER NOT NULL,
    FOREIGN KEY(profile) REFERENCES profiles(id) ON DELETE CASCADE,
    FOREIGN KEY(widget) REFERENCES widgets(id) ON DELETE CASCADE
);

CREATE UNIQUE INDEX unique_layout_entry
    ON profile_layout(profile, widget);