let grid = null;
let socket = null;

// List of widgets that have been added to the grid
let created_widgets = new Set();

// Map of `widget_id` to index in grid
let grid_positions = {};

async function handle_command_output(message) {
    const data = JSON.parse(await message.data.text());

    if (data.widgets) {
        let to_refresh = [];

        for (const [key, value] of Object.entries(data.widgets)) {
            if (!created_widgets.has(key)) {
                // Widget doesn't exist, create and add to grid
                let widget = create_new_widget(key, value);

                grid.add(widget);
                created_widgets.add(key);
            } else {
                // Already exists, just sync changes to inner element
                let element = document.getElementById(key);

                element.setAttribute("duration", value["duration"]);
                element.setAttribute("time", value["time"]);

                // Get muuri content element
                let item_content = element.querySelector(".item-content");
                diff.innerHTML(item_content, value["content"]);

                to_refresh.push(element);
            }
        }

        if (to_refresh.length > 0) {
            grid.refreshItems();
        }

        grid.sort("position");
        grid.layout();
    }
}


function create_new_widget(id, data) {
    console.log("Added new widget", id);

    const widget = document.createElement("div");
    widget.id = id;
    widget.classList.add("item");
    widget.classList.add("fancy-box");

    widget.setAttribute("duration", data["duration"]);
    widget.setAttribute("time", data["time"]);

    const item_content = document.createElement("div");
    item_content.classList.add("item-content");
    widget.appendChild(item_content);

    item_content.innerHTML = data["content"];

    return widget;

}

function replace_link_methods() {
    for (let link of document.links) {
        let method = link.getAttribute("method");
        if (method !== null) {
            link.addEventListener(
                "click",
                (event) => link_on_click(event, link, method),
                true
            );
        }
    }
}

function link_on_click(event, link, method) {
    event.preventDefault();

    let options = {
        method: method
    };

    fetch(link.href, options)
        .then(r => {
            console.log(r);

            if (r.redirected) {
                window.location.href = r.url;
            } else {
                window.location.reload();
            }
        });
}

function handle_move(data) {
    grid.getItems().forEach((item, index) => {
        let widget_id = item._element.id;
        grid_positions[widget_id] = index;
        console.debug(widget_id, index);
    });
    grid.refreshSortData();
    save_layout(grid_positions);
}

function save_layout(positions) {
    if (positions === null) {
        console.warn("Tried to save positions, but positions was null");
        return;
    }

    let profile = get_profile();
    let url = `/profile/${profile}/layout`

    let options = {
        method: "post", body: JSON.stringify(positions), headers: {
            "Content-Type": "application/json"
        }
    };

    console.log("Saving layout", positions);

    fetch(url, options).catch((error) => {
        console.error("Failed to save profile layout to server", error);
    })
}

function load_layout() {
    let profile = get_profile();
    let url = `/profile/${profile}/layout`

    fetch(url)
        .then(response => response.json())
        .then(layout => {
            console.log("Got updated layout", layout)
            grid_positions = layout
        });
}

function get_profile() {
    if (window.location.pathname.startsWith("/profile")) {
        return window.location.pathname.split("/")[2];
    } else {
        return null;
    }
}

function main() {
    // Only run websocket and grid if an app element is present
    if (document.getElementById("app")) {
        console.log("Starting app");
        load_layout();

        socket = new WebSocket("ws://" + location.host + "/ws/1");

        socket.addEventListener("message", handle_command_output);

        grid = new Muuri('.grid', {
            dragEnabled: true,
            sortData: {
                position: function (item, element) {
                    return grid_positions[element.id] ?? -1
                }
            }
        });

        grid.on('dragEnd', handle_move);
    } else {
        console.log("No element with id app found, not starting app");
    }

    if (document.getElementById("tags")) {
        try {
            const tag_input = document.getElementById("tags");
            const tagify = new Tagify(tag_input);

            fetch("/tags")
                .then(response => response.json())
                .then(data => {
                    console.log("Got tags", data);
                    tagify.whitelist = data;
                });
        } catch (e) {
            console.error("Could not create tagify", e);
        }
    }

    replace_link_methods();
}

window.addEventListener("load", main);