use crate::process::ProcessResult;
use hdrhistogram::Histogram;
use std::collections::HashMap;
use std::ops::Deref;
use std::sync::Arc;
use std::time::Instant;
use tokio::sync::Mutex;

/// Stores outputs and stats from widgets
#[derive(Clone)]
pub struct OutputRegistry {
    inner: Arc<Inner>,
}

impl OutputRegistry {
    pub fn new() -> Self {
        Self {
            inner: Arc::new(Inner::new()),
        }
    }
}

impl Deref for OutputRegistry {
    type Target = Inner;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

pub struct Inner {
    /// Map of `(profile, widget_id)` to `output`
    widget_outputs: Mutex<HashMap<(u32, i64), OutputLog>>,
}

impl Inner {
    fn new() -> Self {
        Self {
            widget_outputs: Default::default(),
        }
    }

    pub async fn store_output(&self, profile: u32, widget_id: i64, output: ProcessResult) {
        let mut outputs = self.widget_outputs.lock().await;

        let entry = outputs
            .entry((profile, widget_id))
            .or_insert_with(OutputLog::new);

        entry.record(output)
    }

    pub async fn get_all_last_outputs(&self, profile: u32) -> Vec<(i64, Instant, ProcessResult)> {
        let widgets = self.widget_outputs.lock().await;

        widgets
            .iter()
            .filter(|((widget_profile, _), _)| *widget_profile == profile)
            .filter_map(|((_, id), output)| {
                output
                    .last_result
                    .as_ref()
                    .map(|(time, output)| (*id, *time, output.clone()))
            })
            .collect()
    }

    pub async fn get_last_output(
        &self,
        profile: u32,
        widget_id: i64,
    ) -> Option<(Instant, ProcessResult)> {
        let outputs = self.widget_outputs.lock().await;

        outputs
            .get(&(profile, widget_id))
            .and_then(|log| log.last_result.as_ref())
            .cloned()
    }

    pub async fn get_outputs_since(
        &self,
        profile: u32,
        instant: Instant,
    ) -> Vec<(i64, Instant, ProcessResult)> {
        let outputs = self.widget_outputs.lock().await;

        outputs
            .iter()
            .filter(|((output_profile, _), _)| *output_profile == profile)
            .filter_map(|((_profile, widget), output)| {
                output
                    .last_result
                    .as_ref()
                    .map(|(time, output)| (*widget, *time, output.clone()))
            })
            .filter(|(_widget, time, _output)| time > &instant)
            .collect()
    }
}

struct OutputLog {
    last_result: Option<(Instant, ProcessResult)>,
    /// Histogram of runtimes in ms
    runtimes: Histogram<u64>,
}

impl OutputLog {
    pub fn new() -> Self {
        let runtimes = Histogram::new(4).expect("Failed to create histogram");

        Self {
            last_result: None,
            runtimes,
        }
    }

    pub fn record(&mut self, output: ProcessResult) {
        if let Err(e) = self.runtimes.record(output.duration.as_millis() as u64) {
            tracing::error!(message = "Failed to record widget runtime", error=%e, duration=%output.duration.as_millis());
        }

        self.last_result = Some((Instant::now(), output));
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use speculoos::prelude::*;
    use std::os::unix::prelude::ExitStatusExt;
    use std::process::ExitStatus;
    use std::time::{Duration, SystemTime};
    use test_log::test;

    fn test_process_result() -> ProcessResult {
        ProcessResult {
            time: SystemTime::now(),
            duration: Duration::from_millis(10),
            result: ExitStatus::from_raw(0),
            stdout: String::new(),
            stderr: String::new(),
            structured_output: None,
        }
    }

    #[test(tokio::test)]
    async fn test_can_store_concurrent_results() {
        let registry = OutputRegistry::new();

        futures::future::join_all((0..10).map(|id| {
            let registry = registry.clone();

            async move {
                for x in 0..=10 {
                    let output = ProcessResult {
                        duration: Duration::from_millis(x),
                        ..test_process_result()
                    };
                    registry.store_output(1, id, output).await;
                }
            }
        }))
        .await;

        for widget_id in 0..10 {
            let result = registry.get_last_output(1, widget_id).await;

            asserting!("The final result has been stored")
                .that(&result)
                .is_some()
                .map(|(_time, result)| &result.duration)
                // Wait time is equal to x in test, last result should be 10
                .is_equal_to(Duration::from_millis(10));
        }
    }

    #[test(tokio::test)]
    async fn test_can_get_outputs() {
        let registry = OutputRegistry::new();

        {
            let result = registry.get_outputs_since(1, Instant::now()).await;
            asserting!("Empty registry has no results")
                .that(&result)
                .is_empty();
        }

        {
            let check_time = Instant::now();
            registry.store_output(1, 1, test_process_result()).await;

            let result = registry.get_outputs_since(1, check_time).await;
            asserting!("There is a now a single result in the registry")
                .that(&result)
                .has_length(1);

            asserting!("Entry has correct widget_id")
                .that(&result[0].0)
                .is_equal_to(1);
        }

        {
            let result = registry.get_outputs_since(1, Instant::now()).await;

            asserting!("Registry doesn't return anything, since there are no new entries")
                .that(&result)
                .is_empty();
        }
    }
}
