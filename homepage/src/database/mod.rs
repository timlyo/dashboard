use sqlx::sqlite::SqliteConnectOptions;
use sqlx::{ConnectOptions, SqlitePool};
use tracing::log::LevelFilter;

pub mod profiles;
pub mod tags;
pub mod widgets;

pub async fn get_db() -> Result<SqlitePool, sqlx::Error> {
    let mut options = SqliteConnectOptions::new()
        .filename("homepage.sqlite")
        .create_if_missing(true);

    options.log_statements(LevelFilter::Debug);

    sqlx::sqlite::SqlitePool::connect_with(options).await
}
