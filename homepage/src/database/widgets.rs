use crate::forms;
use sqlx::sqlite::SqliteQueryResult;
use sqlx::SqlitePool;
use std::collections::HashMap;

#[tracing::instrument(skip(db))]
pub async fn get_widget_by_id(db: &SqlitePool, id: i64) -> Result<Option<Widget>, sqlx::Error> {
    sqlx::query_as!(
        Widget,
        // language=SQL
        r#"SELECT id, command, args FROM widgets WHERE id = ?"#,
        id
    )
    .fetch_optional(db)
    .await
}

#[tracing::instrument(skip(db))]
pub async fn get_all_widgets(db: &SqlitePool) -> Result<Vec<Widget>, sqlx::Error> {
    sqlx::query_as!(
        Widget,
        // language=SQL
        r#"SELECT id, command, args as "args:_" FROM widgets"#
    )
    .fetch_all(db)
    .await
}

pub async fn update_widget(
    db: &SqlitePool,
    widget_id: i64,
    widget: &forms::Widget,
) -> Result<SqliteQueryResult, sqlx::Error> {
    sqlx::query!(
        // language=SQL
        r#"UPDATE widgets 
            SET command = ?, args = ? 
            WHERE id = ?"#,
        widget.command,
        widget.args,
        widget_id,
    )
    .execute(db)
    .await
}

#[tracing::instrument(skip(db))]
pub async fn get_widgets_for_tags(
    db: &SqlitePool,
    tags: &[i64],
) -> Result<Vec<Widget>, sqlx::Error> {
    let widgets: Vec<Vec<Widget>> =
        futures::future::try_join_all(tags.iter().map(|tag| async move {
            sqlx::query_as!(
                Widget,
                // language=SQL
                r#"SELECT DISTINCT(id), command, args FROM widget_tags
                    JOIN widgets ON widgets.id = widget_tags.widget
                    WHERE tag = ? "#,
                tag
            )
            .fetch_all(db)
            .await
        }))
        .await?;

    Ok(widgets.into_iter().flatten().collect())
}

#[tracing::instrument(skip(db))]
pub async fn add_widget(db: &SqlitePool, command: &str, args: &str) -> Result<i64, sqlx::Error> {
    tracing::info!("Adding widget");

    sqlx::query_scalar!(
        // language=SQL
        "INSERT INTO widgets (command, args) VALUES (?, ?) RETURNING id",
        command,
        args
    )
    .fetch_one(db)
    .await
}

#[tracing::instrument(skip(db))]
pub async fn delete_widget(
    db: &SqlitePool,
    widget_id: u32,
) -> Result<SqliteQueryResult, sqlx::Error> {
    sqlx::query!(
        // language=SQL
        "DELETE FROM widgets WHERE id = ?",
        widget_id
    )
    .execute(db)
    .await
}

#[derive(Debug)]
pub struct Widget {
    pub id: i64,
    pub command: String,
    pub args: Option<String>,
}

impl Widget {
    pub fn parse_args(&self) -> Result<Vec<String>, shell_words::ParseError> {
        self.args
            .as_deref()
            .map(shell_words::split)
            .transpose()
            .map(|args: Option<Vec<_>>| args.unwrap_or_default())
    }
}

struct WidgetStateEntry {
    pub key: String,
    pub value: Option<String>,
}

impl WidgetStateEntry {
    pub fn parse_state(&self) -> serde_json::Value {
        if let Some(ref value) = self.value {
            serde_json::from_str(value).unwrap()
        } else {
            serde_json::Value::Null
        }
    }
}

#[tracing::instrument(skip(db))]
pub async fn get_widget_state(
    db: &SqlitePool,
    widget_id: i64,
) -> Result<HashMap<String, serde_json::Value>, sqlx::Error> {
    let state: Vec<WidgetStateEntry> = sqlx::query_as!(
        WidgetStateEntry,
        // language=SQL
        r#"SELECT key, value as "value:_" FROM widget_state WHERE widget = ?"#,
        widget_id
    )
    .fetch_all(db)
    .await?;

    Ok(state
        .into_iter()
        .map(|state| {
            let value = state.parse_state();
            (state.key, value)
        })
        .collect())
}
