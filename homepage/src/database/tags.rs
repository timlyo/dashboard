use sqlx::sqlite::SqliteQueryResult;
use sqlx::{Sqlite, SqlitePool, Transaction};

#[tracing::instrument(skip(db))]
pub async fn get_all_tags(db: &SqlitePool) -> Result<Vec<String>, sqlx::Error> {
    sqlx::query_scalar!(
        // language=SQL
        "SELECT name FROM tags"
    )
    .fetch_all(db)
    .await
}

#[tracing::instrument(skip(db), err)]
pub async fn insert_tag(
    db: &mut Transaction<'_, Sqlite>,
    tag_name: &str,
) -> Result<i64, sqlx::Error> {
    sqlx::query_scalar!(
        // language=SQL
        r#"INSERT INTO tags (name) VALUES (?) 
            ON CONFLICT (name) DO UPDATE SET name = excluded.name
            RETURNING id as "id!""#,
        tag_name
    )
    .fetch_one(db)
    .await
}

#[tracing::instrument(skip(db))]
pub async fn get_tags_for_widget(
    db: &SqlitePool,
    widget_id: i64,
) -> Result<Vec<(i64, String)>, sqlx::Error> {
    sqlx::query!(
        // language=SQL
        r#"SELECT tags.id, tags.name FROM widget_tags 
            JOIN tags ON widget_tags.tag = tags.id
            WHERE widget = ?"#,
        widget_id
    )
    .map(|r| (r.id, r.name))
    .fetch_all(db)
    .await
}

#[tracing::instrument(skip(db))]
pub async fn add_tag_to_widget(
    db: &SqlitePool,
    widget_id: i64,
    tag_id: i64,
) -> Result<SqliteQueryResult, sqlx::Error> {
    sqlx::query!(
        // language=SQL
        "INSERT INTO widget_tags (widget, tag) VALUES (?, ?)",
        widget_id,
        tag_id
    )
    .execute(db)
    .await
}

#[tracing::instrument(skip(db))]
pub async fn add_widget_tags(
    db: &SqlitePool,
    widget_id: i64,
    tags: &[String],
) -> Result<Vec<SqliteQueryResult>, sqlx::Error> {
    tracing::info!("Adding widget tags");

    futures::future::try_join_all(tags.iter().map(|tag| {
        let widget_id = widget_id;
        async move {
            let tag_id: i64 = sqlx::query_scalar!(
                //language=SQL
                r#"INSERT INTO tags (name) VALUES (?) 
                    ON CONFLICT (name) DO UPDATE SET name = excluded.name
                    RETURNING id as "id!""#,
                tag
            )
            .fetch_one(db)
            .await?;

            sqlx::query!(
                // language=SQL
                "INSERT INTO widget_tags (widget, tag) VALUES (?, ?)",
                widget_id,
                tag_id
            )
            .execute(db)
            .await
        }
    }))
    .await
}
