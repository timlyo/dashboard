use sqlx::sqlite::SqliteQueryResult;
use sqlx::{Sqlite, SqlitePool, Transaction};
use std::collections::HashMap;

#[tracing::instrument(skip(db))]
pub async fn create_new_profile(db: &SqlitePool, name: &str) -> Result<i64, sqlx::Error> {
    sqlx::query_scalar!(
        // language=SQL
        "INSERT INTO profiles (name) VALUES (?) RETURNING id",
        name
    )
    .fetch_one(db)
    .await
}

#[tracing::instrument(skip(db))]
pub async fn get_profile_list(db: &SqlitePool) -> Result<Vec<Profile>, sqlx::Error> {
    sqlx::query_as!(
        Profile,
        // language=SQL
        "SELECT name, id FROM profiles"
    )
    .fetch_all(db)
    .await
}

pub async fn get_profile_by_id(db: &SqlitePool, profile_id: u32) -> Result<Profile, sqlx::Error> {
    sqlx::query_as!(
        Profile,
        // language=SQL
        "SELECT id, name FROM profiles WHERE id = ?",
        profile_id
    )
    .fetch_one(db)
    .await
}

#[tracing::instrument(skip(db))]
pub async fn get_profile_tag_list(
    db: &SqlitePool,
    profile_id: u32,
) -> Result<Vec<(i64, String)>, sqlx::Error> {
    sqlx::query!(
        // language=SQL
        "SELECT id, name FROM tags
            JOIN profile_tags ON tags.id = profile_tags.tag
            WHERE profile_tags.profile = ?",
        profile_id
    )
    .map(|r| (r.id, r.name))
    .fetch_all(db)
    .await
}

#[tracing::instrument(skip(db))]
pub async fn delete_profile_tags(
    db: &mut Transaction<'_, Sqlite>,
    profile_id: u32,
) -> Result<SqliteQueryResult, sqlx::Error> {
    sqlx::query!(
        // language=SQL
        "DELETE FROM profile_tags WHERE profile = ?",
        profile_id
    )
    .execute(db)
    .await
}

#[tracing::instrument(skip(db))]
pub async fn update_profile(
    db: &mut Transaction<'_, Sqlite>,
    profile_id: u32,
    profile_name: &str,
) -> Result<SqliteQueryResult, sqlx::Error> {
    sqlx::query!(
        // language=SQL
        "UPDATE profiles SET name = ? WHERE id = ?",
        profile_name,
        profile_id,
    )
    .execute(db)
    .await
}

#[tracing::instrument(skip(db), err)]
pub async fn add_tag_to_profile(
    db: &mut Transaction<'_, Sqlite>,
    tag_id: i64,
    profile: u32,
) -> Result<SqliteQueryResult, sqlx::Error> {
    sqlx::query!(
        // language=SQL
        "INSERT INTO profile_tags (profile, tag) VALUES (?, ?)",
        profile,
        tag_id
    )
    .execute(db)
    .await
}

pub struct Profile {
    pub name: String,
    pub id: i64,
}

pub async fn save_layout(
    db: &mut Transaction<'_, Sqlite>,
    profile_id: u32,
    layout: HashMap<i64, u32>,
) -> Result<(), sqlx::Error> {
    for (widget, position) in layout {
        sqlx::query!(
            // language=SQL
            "INSERT INTO profile_layout(profile, widget, position)
                VALUES (?,?,?)
                ON CONFLICT (profile, widget) DO UPDATE SET position=excluded.position",
            profile_id,
            widget,
            position
        )
        .execute(&mut *db)
        .await?;
    }

    Ok(())
}

pub async fn get_layout(db: &SqlitePool, profile_id: u32) -> Result<Vec<(i64, i64)>, sqlx::Error> {
    sqlx::query!(
        // language=SQL
        "SELECT widget, position FROM profile_layout
            WHERE profile = ?",
        profile_id
    )
    .map(|r| (r.widget, r.position))
    .fetch_all(db)
    .await
}
