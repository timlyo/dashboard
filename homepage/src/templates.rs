use crate::database::profiles::Profile;
use crate::database::widgets::Widget;
use askama_axum::Template;
use std::collections::HashMap;

#[derive(Template)]
#[template(path = "index.html")]
pub struct Index {
    pub profiles: Vec<Profile>,
}

#[derive(Template)]
#[template(path = "new_profile.html")]
pub struct NewProfile {}

#[derive(Template)]
#[template(path = "main.html")]
pub struct Main {
    pub profile_id: u32,
    pub profile_name: String,
}

#[derive(Template)]
#[template(path = "widgets.html")]
pub struct Widgets {
    pub profile_id: u32,
    pub profile_tags: Vec<(i64, String)>,
    pub profile_name: String,
    pub widgets: Vec<Widget>,
}

#[derive(Template)]
#[template(path = "widget.html")]
pub struct WidgetPage {
    pub widget: Widget,
    pub state: HashMap<String, serde_json::Value>,
    pub tags: Vec<String>,
}

#[derive(Template)]
#[template(path = "edit_profile.html")]
pub struct EditProfile {
    pub profile_id: u32,
    pub profile_name: String,
    pub current_tags: Vec<String>,
}
