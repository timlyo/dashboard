use axum::body;
use axum::http::StatusCode;
use axum::response::{IntoResponse, Response};
use std::fmt;
use std::fmt::{Debug, Display, Formatter};
use std::string::FromUtf8Error;
use tracing_error::TracedError;

#[derive(Debug, thiserror::Error)]
pub enum Kind {
    #[error("IO Error {0}")]
    IoError(#[from] std::io::Error),
    #[error("Non UTF8 output {0}")]
    NonUtf8Response(#[from] FromUtf8Error),
    #[error("Invalid JSON {0}")]
    InvalidJson(#[from] serde_json::Error),
    #[error("Database error: {0}")]
    Database(#[from] sqlx::Error),
    #[error("Couldn't find a {resource} with {id}")]
    NotFound { resource: &'static str, id: String },
    #[error("Invalid widget arguments: {0}")]
    InvalidArgs(#[from] shell_words::ParseError),
    #[error("Error in config {0}")]
    Config(#[from] figment::Error)
}

impl Kind {
    pub fn as_status(&self) -> StatusCode {
        match self {
            Kind::IoError(_) => StatusCode::INTERNAL_SERVER_ERROR,
            Kind::NonUtf8Response(_) => StatusCode::INTERNAL_SERVER_ERROR,
            Kind::InvalidJson(_) => StatusCode::INTERNAL_SERVER_ERROR,
            Kind::Database(_) => StatusCode::INTERNAL_SERVER_ERROR,
            Kind::NotFound { .. } => StatusCode::NOT_FOUND,
            Kind::InvalidArgs(_) => StatusCode::BAD_REQUEST,
            Kind::Config(_) => StatusCode::INTERNAL_SERVER_ERROR
        }
    }
}

#[derive(Debug)]
pub struct Error {
    source: TracedError<Kind>,
    status: StatusCode,
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        fmt::Display::fmt(&self.source, f)
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        self.source.source()
    }
}

impl IntoResponse for Error {
    fn into_response(self) -> Response {
        let code = std::error::Error::source(&self)
            .and_then(|s| s.downcast_ref::<Error>())
            .map(|e| e.status)
            .unwrap_or(StatusCode::INTERNAL_SERVER_ERROR);

        let body = body::boxed(body::Full::from(self.to_string()));

        Response::builder().status(code).body(body).unwrap()
    }
}

impl<E> From<E> for Error
where
    Kind: From<E>,
{
    fn from(source: E) -> Self {
        let source = Kind::from(source);

        Self {
            status: source.as_status(),
            source: source.into(),
        }
    }
}
