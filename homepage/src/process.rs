use crate::error::Error;
use homepage::StructuredOutput;
use serde_json::json;
use std::collections::HashMap;
use std::ffi::OsStr;
use std::process::{ExitStatus, Output, Stdio};
use std::time::{Duration, Instant, SystemTime};
use tokio::io::AsyncWriteExt;

#[derive(Debug, Clone)]
pub struct ProcessResult {
    pub time: SystemTime,
    pub duration: Duration,
    pub result: ExitStatus,
    pub stdout: String,
    pub stderr: String,
    pub structured_output: Option<StructuredOutput>,
}

impl ProcessResult {
    pub fn new(result: Output, runtime: Duration) -> Self {
        let stdout = String::from_utf8_lossy(&result.stdout).to_string();
        let stderr = String::from_utf8_lossy(&result.stderr).to_string();

        let structured_output = serde_json::from_str(&stdout).ok();

        Self {
            duration: runtime,
            time: SystemTime::now(),
            result: result.status,
            stdout,
            stderr,
            structured_output,
        }
    }
}

#[tracing::instrument(skip(command))]
pub async fn run_command(
    command: impl AsRef<OsStr>,
    args: Vec<String>,
    public_state: HashMap<String, serde_json::Value>,
) -> Result<ProcessResult, Error> {
    let input = json!({
        "state": public_state,
    });

    let start_time = Instant::now();

    let mut process = tokio::process::Command::new(command.as_ref())
        .args(args.iter())
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()?;

    if let Some(stdin) = process.stdin.as_mut() {
        let value = serde_json::to_string_pretty(&input)?;
        stdin.write_all(value.as_bytes()).await?;
    } else {
        let command = command.as_ref().to_string_lossy();
        tracing::error!(message="No stdin for command", %command);
    }

    let result = process.wait_with_output().await?;

    let runtime = Instant::now() - start_time;

    Ok(ProcessResult::new(result, runtime))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn test_run_command() {
        let output = run_command("echo", vec!["test".into()], HashMap::new())
            .await
            .unwrap();

        assert_eq!(output.stdout, "test\n");
    }
}
