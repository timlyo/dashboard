//! Runs commands on a regular schedule. Has one instance per connected profile

use crate::database::widgets;
use crate::error::Error;
use crate::output_registry::OutputRegistry;
use sqlx::{Pool, Sqlite};
use std::collections::HashMap;
use std::fmt::Display;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::time::{Duration, Instant, SystemTime};
use tokio::sync::Mutex;
use tokio::task::JoinHandle;

/// Stores all schedulers for all profiles
#[derive(Clone)]
pub struct SchedulerRegistry {
    schedulers: Arc<Mutex<HashMap<u32, Arc<Scheduler>>>>,
    output_registry: OutputRegistry,
    db: sqlx::SqlitePool,
}

impl SchedulerRegistry {
    pub fn new(db: sqlx::SqlitePool, output_registry: OutputRegistry) -> Self {
        Self {
            schedulers: Arc::new(Mutex::new(HashMap::new())),
            output_registry,
            db,
        }
    }

    pub async fn get_scheduler(&mut self, profile: u32) -> Arc<Scheduler> {
        let mut schedulers = self.schedulers.lock().await;

        schedulers
            .entry(profile)
            .or_insert_with(|| {
                Arc::new(Scheduler::new(
                    profile,
                    self.db.clone(),
                    self.output_registry.clone(),
                ))
            })
            .clone()
    }
}

#[derive(Debug)]
pub struct WidgetInstance {
    id: i64,
    command: String,
    args: Vec<String>,
}

pub struct Scheduler {
    created: SystemTime,
    running: Arc<AtomicBool>,
    output_registry: OutputRegistry,
    pub profile: u32,
    widgets: Arc<Mutex<HashMap<i64, WidgetInstance>>>,
    /// Last time that results were collected from this scheduler
    last_check: Arc<Mutex<Instant>>,
    db: sqlx::SqlitePool,
}

impl Scheduler {
    pub fn new(profile: u32, db: sqlx::SqlitePool, output_registry: OutputRegistry) -> Self {
        tracing::info!(message="Created new scheduler", %profile);

        Self {
            created: SystemTime::now(),
            widgets: Arc::new(Mutex::new(HashMap::new())),
            running: Arc::new(AtomicBool::new(false)),
            output_registry,
            profile,
            last_check: Arc::new(Mutex::new(Instant::now())),
            db,
        }
    }

    pub async fn add_widget(&self, id: i64, command: impl Display, args: Vec<String>) -> i64 {
        tracing::info!(message="Adding widget", %command, ?args, id);

        let instance = WidgetInstance {
            id,
            command: command.to_string(),
            args,
        };

        let mut widgets = self.widgets.lock().await;

        widgets.insert(id, instance);

        id
    }

    /// Starts a long running task that monitors all programs for widgets
    #[tracing::instrument(skip(self), fields(profile=self.profile))]
    pub fn monitor(&self) -> JoinHandle<Result<(), Error>> {
        self.running.store(true, Ordering::SeqCst);

        let profile = self.profile;
        let widgets = self.widgets.clone();
        let running = self.running.clone();
        let last_check = self.last_check.clone();
        let db = self.db.clone();
        let created = self.created;
        let output_registry = self.output_registry.clone();

        tracing::info!(message = "Starting monitor");

        tokio::task::spawn(async move {
            while running.load(Ordering::SeqCst) {
                {
                    let widgets = widgets.lock().await;

                    tracing::debug!(message = "Running widget commands", widgets = widgets.len());

                    futures::future::join_all(widgets.values().into_iter().map(|widget| {
                        run_command_for_widget(&output_registry, profile, &db, widget)
                    }))
                    .await;
                }

                {
                    let last_check = Instant::now() - *last_check.lock().await;

                    if last_check > Duration::from_secs(60) {
                        tracing::info!(message = "Last check was more than a minute ago", %profile);
                        running.store(false, Ordering::SeqCst);
                    }
                }

                tokio::time::sleep(tokio::time::Duration::from_millis(50)).await;
            }

            let runtime = SystemTime::now().duration_since(created);
            tracing::info!(message = "Done with scheduler", ?runtime);
            Ok(())
        })
    }
}

#[tracing::instrument(skip(output_registry, db), err)]
async fn run_command_for_widget(
    output_registry: &OutputRegistry,
    profile_id: u32,
    db: &Pool<Sqlite>,
    widget: &WidgetInstance,
) -> Result<(), Error> {
    let args = widget.args.clone();

    // Skip this run if last run was too recent
    let last_run = output_registry.get_last_output(profile_id, widget.id).await;
    if let Some((time, result)) = last_run {
        let interval = result
            .structured_output
            .and_then(|s| s.rerun_duration())
            .unwrap_or_else(|| Duration::from_millis(5000));

        if (time + interval) > Instant::now() {
            return Ok(());
        }
    }

    let state = widgets::get_widget_state(db, widget.id).await?;

    let result = crate::process::run_command(&widget.command, args, state).await?;

    tracing::debug!(message="Ran command", time_ms=%result.duration.as_millis(), structured_output=%result.structured_output.is_some());
    tracing::trace!(message = "Output", ?result);

    if !result.result.success() {
        tracing::error!(
            message = "Error response from command",
            command=%widget.command,
            status=%result.result,
            stdout=%result.stdout,
            stderr=%result.stderr
        );
    }

    output_registry
        .store_output(profile_id, widget.id, result)
        .await;

    Ok(())
}

#[cfg(test)]
mod tests {
    use crate::output_registry::OutputRegistry;
    use crate::scheduler::Scheduler;
    use speculoos::prelude::*;
    use std::time::Duration;
    use test_log::test;

    #[test(tokio::test)]
    async fn scheduler_runs_process() {
        let db = homepage::get_test_database().await;
        let output_registry = OutputRegistry::new();

        let profile = 1;
        let id = 0;

        let scheduler = Scheduler::new(profile, db, output_registry.clone());

        scheduler.add_widget(id, "date", Vec::new()).await;

        scheduler.monitor();

        tokio::time::sleep(Duration::from_millis(100)).await;

        let output = output_registry.get_last_output(profile, id).await;

        asserting!("Command has been run").that(&output).is_some();
    }
}
