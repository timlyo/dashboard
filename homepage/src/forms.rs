#[derive(serde::Deserialize, Debug)]
pub struct NewProfile {
    pub name: String,
}

/// Tagify value
#[derive(serde::Deserialize, Debug)]
struct Tag {
    value: String,
}

#[derive(serde::Deserialize, Debug)]
pub struct Widget {
    pub command: String,
    pub args: String,
    pub tags: String,
}

impl Widget {
    pub fn parse_tags(&self) -> Result<Vec<String>, serde_json::Error> {
        serde_json::from_str::<Vec<Tag>>(&self.tags)
            .map(|tags| tags.into_iter().map(|tag| tag.value).collect())
    }
}

#[derive(serde::Deserialize, Debug)]
pub struct Profile {
    pub name: String,
    pub tags: String,
}

impl Profile {
    pub fn parse_tags(&self) -> Result<Vec<String>, serde_json::Error> {
        serde_json::from_str::<Vec<Tag>>(&self.tags)
            .map(|tags| tags.into_iter().map(|tag| tag.value).collect())
    }
}
