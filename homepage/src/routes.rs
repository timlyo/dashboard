use crate::database::{profiles, tags, widgets};
use crate::error::Error;
use crate::forms::Profile;
use crate::{
    database, error, templates, Command, Index, NewProfile, OutputRegistry, Response,
    SchedulerRegistry, Widget,
};
use axum::extract::{Extension, Form, OriginalUri, Path};
use axum::handler::Handler;
use axum::http::StatusCode;
use axum::response::{Headers, IntoResponse, Redirect};
use axum::routing::{get, post};
use axum::{Json, Router};
use axum_typed_websockets::WebSocketUpgrade;
use itertools::Itertools;
use std::collections::HashMap;

pub fn router() -> Router {
    let profile_router = Router::new()
        .route("/", get(profile))
        .route("/edit", get(edit_profile_page).post(edit_profile))
        .route("/widgets", get(widgets_page))
        .route("/layout", get(get_profile_layout).post(save_profile_layout));

    Router::new()
        .nest("/profile/:profile_id", profile_router)
        .route("/", get(index))
        .route("/new_profile", get(new_profile_page).post(new_profile))
        .route("/widgets", post(add_widget))
        .route(
            "/widgets/:widget_id",
            get(widget_page).delete(delete_widget).post(edit_widget),
        )
        .route("/script.js", get(script))
        .route("/style.css", get(style))
        .route("/background.jpg", get(background))
        .route("/ws/:profile", get(websocket))
        .route("/tags", get(tags))
        .fallback(not_found.into_service())
}

async fn not_found(OriginalUri(uri): OriginalUri) -> impl IntoResponse {
    (StatusCode::NOT_FOUND, format!("Nothing found at {uri}"))
}

async fn index(Extension(db): Extension<sqlx::SqlitePool>) -> templates::Index {
    let profiles = profiles::get_profile_list(&db).await.unwrap();

    Index { profiles }
}

async fn profile(
    Extension(db): Extension<sqlx::SqlitePool>,
    Extension(mut scheduler): Extension<SchedulerRegistry>,
    Path(profile_id): Path<u32>,
) -> Result<templates::Main, Error> {
    let scheduler = scheduler.get_scheduler(profile_id).await;

    let profile = profiles::get_profile_by_id(&db, profile_id).await?;
    let tags = profiles::get_profile_tag_list(&db, profile_id).await?;

    let tag_ids = tags.iter().map(|(id, _name)| *id).collect_vec();
    let widgets = database::widgets::get_widgets_for_tags(&db, &tag_ids).await?;

    tracing::info!(message = "Loaded profile", id=%profile_id, ?tags, ?widgets);

    for widget in widgets {
        let args = widget.parse_args()?;
        scheduler.add_widget(widget.id, widget.command, args).await;
    }

    scheduler.monitor();

    Ok(templates::Main {
        profile_id,
        profile_name: profile.name,
    })
}

async fn new_profile_page() -> templates::NewProfile {
    templates::NewProfile {}
}

async fn new_profile(
    Extension(db): Extension<sqlx::SqlitePool>,
    form: Form<NewProfile>,
) -> Result<Redirect, String> {
    let id = profiles::create_new_profile(&db, &form.name)
        .await
        .map_err(|e| e.to_string())?;

    Ok(Redirect::to(format!("/profile/{}", id).parse().unwrap()))
}

async fn edit_profile_page(
    Path(profile_id): Path<u32>,
    Extension(db): Extension<sqlx::SqlitePool>,
) -> Result<templates::EditProfile, Error> {
    let profile = profiles::get_profile_by_id(&db, profile_id).await?;
    let current_tags = profiles::get_profile_tag_list(&db, profile_id)
        .await?
        .into_iter()
        .map(|(_id, name)| name)
        .collect();

    Ok(templates::EditProfile {
        profile_id,
        profile_name: profile.name,
        current_tags,
    })
}

async fn edit_profile(
    Path(profile_id): Path<u32>,
    Extension(db): Extension<sqlx::SqlitePool>,
    Form(profile): Form<Profile>,
) -> Result<Redirect, Error> {
    tracing::info!(message = "Updating profile", %profile_id, name=%profile.name, tags=?profile.tags);

    let mut transaction = db.begin().await?;

    profiles::update_profile(&mut transaction, profile_id, &profile.name).await?;
    profiles::delete_profile_tags(&mut transaction, profile_id).await?;

    for tag in profile.parse_tags()? {
        let tag_id = tags::insert_tag(&mut transaction, &tag).await?;
        profiles::add_tag_to_profile(&mut transaction, tag_id, profile_id).await?;
    }

    transaction.commit().await?;

    Ok(Redirect::to(
        format!("/profile/{}", profile_id).parse().unwrap(),
    ))
}

async fn get_profile_layout(
    Path(profile_id): Path<u32>,
    Extension(db): Extension<sqlx::SqlitePool>,
) -> Result<Json<HashMap<String, i64>>, Error> {
    let layout = database::profiles::get_layout(&db, profile_id).await?;

    Ok(Json(
        layout
            .into_iter()
            .map(|(widget, position)| (format!("widget-{widget}"), position))
            .collect(),
    ))
}

async fn save_profile_layout(
    Path(profile_id): Path<u32>,
    Extension(db): Extension<sqlx::SqlitePool>,
    Json(layout): Json<HashMap<String, u32>>,
) -> Result<StatusCode, Error> {
    let mut transaction = db.begin().await?;

    let layout = layout
        .into_iter()
        .map(|(key, value)| (key.split_once('-').unwrap().1.parse().unwrap(), value))
        .collect();

    tracing::info!(message = "New layout", ?layout);

    database::profiles::save_layout(&mut transaction, profile_id, layout).await?;

    transaction.commit().await?;

    Ok(StatusCode::OK)
}

async fn tags(Extension(db): Extension<sqlx::SqlitePool>) -> Result<Json<Vec<String>>, Error> {
    match tags::get_all_tags(&db).await {
        Ok(tags) => Ok(Json(tags)),
        Err(e) => Err(Error::from(e)),
    }
}

#[tracing::instrument(skip(db))]
async fn widget_page(
    Path(widget_id): Path<i64>,
    Extension(db): Extension<sqlx::SqlitePool>,
) -> Result<templates::WidgetPage, Error> {
    let widget = database::widgets::get_widget_by_id(&db, widget_id).await?;
    let state = widgets::get_widget_state(&db, widget_id).await?;
    let tags = tags::get_tags_for_widget(&db, widget_id)
        .await?
        .into_iter()
        .map(|(_id, name)| name)
        .collect();

    tracing::info!(message = "Loaded widget", ?widget, ?tags);

    if let Some(widget) = widget {
        Ok(templates::WidgetPage {
            widget,
            state,
            tags,
        })
    } else {
        Err(error::Kind::NotFound {
            id: widget_id.to_string(),
            resource: "widget",
        }
        .into())
    }
}

async fn widgets_page(
    Path(profile_id): Path<u32>,
    Extension(db): Extension<sqlx::SqlitePool>,
) -> Result<templates::Widgets, Error> {
    let profile_name = profiles::get_profile_by_id(&db, profile_id).await?.name;
    let profile_tags = profiles::get_profile_tag_list(&db, profile_id).await?;
    let widgets = database::widgets::get_all_widgets(&db).await?;

    Ok(templates::Widgets {
        profile_id,
        profile_name,
        profile_tags,
        widgets,
    })
}

#[tracing::instrument(skip(db))]
async fn edit_widget(
    Path(widget_id): Path<i64>,
    Extension(db): Extension<sqlx::SqlitePool>,
    Form(widget): Form<Widget>,
) -> Result<Redirect, Error> {
    if database::widgets::get_widget_by_id(&db, widget_id)
        .await?
        .is_none()
    {
        return Err(error::Kind::NotFound {
            id: widget_id.to_string(),
            resource: "widget",
        }
        .into());
    }

    tracing::info!(message = "Updating widget");

    let tag_names = widget.parse_tags()?;

    database::widgets::update_widget(&db, widget_id, &widget).await?;
    database::tags::add_widget_tags(&db, widget_id, &tag_names).await?;

    Ok(Redirect::to(
        format!("/widgets/{}", widget_id).parse().unwrap(),
    ))
}

async fn add_widget(
    Extension(db): Extension<sqlx::SqlitePool>,
    form: Form<Widget>,
) -> Result<Redirect, Error> {
    let id = database::widgets::add_widget(&db, &form.command, &form.args).await?;

    tags::add_widget_tags(&db, id, &form.parse_tags()?).await?;

    // TODO redirect somewhere useful
    Ok(Redirect::to("/".parse().unwrap()))
}

async fn delete_widget(
    Extension(db): Extension<sqlx::SqlitePool>,
    Path(widget_id): Path<u32>,
) -> impl IntoResponse {
    database::widgets::delete_widget(&db, widget_id)
        .await
        .map(|_| StatusCode::NO_CONTENT)
        .map_err(|e| e.to_string())
}

async fn script() -> impl IntoResponse {
    let headers = Headers(vec![("content-type", "text/javascript")]);

    (StatusCode::OK, headers, include_str!("../assets/script.js"))
}

async fn style() -> impl IntoResponse {
    let headers = Headers(vec![("content-type", "text/css")]);

    (StatusCode::OK, headers, include_str!("../assets/style.css"))
}

async fn background() -> impl IntoResponse {
    let headers = Headers(vec![("content-type", "image/jpg")]);
    let image = include_bytes!("../assets/background.jpg").to_vec();

    (StatusCode::OK, headers, image)
}

#[tracing::instrument(skip(ws, output_registry))]
async fn websocket(
    ws: WebSocketUpgrade<Response, Command>,
    Path(profile): Path<u32>,
    Extension(output_registry): Extension<OutputRegistry>,
) -> impl IntoResponse {
    tracing::info!(message = "Websocket connection established", %profile);

    ws.on_upgrade(move |socket| crate::handle_socket(socket, profile, output_registry))
}
