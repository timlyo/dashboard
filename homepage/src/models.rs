use crate::widget_processor::RenderOutput;
use std::collections::HashMap;
use std::fmt::Debug;
use std::io::Error;
use std::process::Output;

#[derive(serde::Serialize, Debug)]
pub(crate) struct Response {
    pub widgets: HashMap<String, RenderOutput>,
    pub error: Option<String>,
}

impl From<std::io::Error> for Response {
    fn from(error: Error) -> Self {
        Response {
            widgets: HashMap::new(),
            error: Some(error.to_string()),
        }
    }
}

impl From<Output> for Response {
    fn from(_output: Output) -> Self {
        Response {
            widgets: HashMap::new(),
            error: None,
        }
    }
}

#[derive(serde::Serialize, Debug)]
pub struct CommandOutput {
    pub status: Option<i32>,
    pub stdout: String,
    pub stderr: String,
    pub json: Option<serde_json::Value>,
}

#[derive(serde::Deserialize, Debug)]
pub struct Command {
    pub event: Event,
}

#[derive(serde::Deserialize, Debug)]
pub enum Event {
    Init,
}
