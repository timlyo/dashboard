#![warn(missing_docs)]

//! A library for creating widgets with Homepage

extern crate core;

pub use public_models::*;

/// A Connection to an in-memory copy of an empty database with tables setup
pub async fn get_test_database() -> sqlx::SqlitePool {
    let db = sqlx::SqlitePool::connect(":memory:").await.unwrap();

    sqlx::migrate!("../migrations").run(&db).await.unwrap();

    db
}

/// Parses stdin to get the data passed to the command
pub fn parse_stdin() -> Result<CommandInput, serde_json::Error> {
    serde_json::from_reader(std::io::stdin())
}

mod public_models {
    use std::time::Duration;

    /// Structure of the data passed to a widget via stdin
    #[derive(serde::Deserialize, serde::Serialize, Debug, Clone)]
    pub struct CommandInput {
        state: serde_json::Value,
    }

    #[allow(dead_code)]
    impl CommandInput {
        /// Parses the input from stdin
        pub fn get_state<T>(&self) -> Result<T, serde_json::Error>
        where
            T: serde::de::DeserializeOwned,
        {
            serde_json::from_value(self.state.clone())
        }
    }

    /// This can be printed to stdout when a command requires a more structured output
    /// e.g. Control how often the command is called, modify the command's state
    #[derive(serde::Deserialize, serde::Serialize, Debug, Clone, Default)]
    pub struct StructuredOutput {
        pub(crate) widget_content: WidgetContent,
        pub(crate) rerun_in_ms: Option<u128>,
    }

    /// Type of content that this widget is displaying
    #[derive(serde::Deserialize, serde::Serialize, Debug, Clone)]
    pub enum WidgetContent {
        /// Content will be displayed vebatim as HTML
        Html(String),
        /// Content will be converted to html. e.g. \n will become <br>
        Text(String),
        /// An empty widget won't be displayed anywhere, but can still change states
        Empty,
    }

    impl Default for WidgetContent {
        fn default() -> Self {
            WidgetContent::Empty
        }
    }

    impl StructuredOutput {
        /// Create a new empty instance
        pub fn new() -> Self {
            Self::default()
        }

        /// Return the widget's raw content as a str
        pub fn widget_content(&self) -> Option<&str> {
            match self.widget_content {
                WidgetContent::Text(ref content) | WidgetContent::Html(ref content) => {
                    Some(content.as_str())
                }
                WidgetContent::Empty => None,
            }
        }

        /// Prints the output to stdout, or any errors to stderr
        pub fn print(&self) {
            let content = serde_json::to_string_pretty(self);

            match content {
                Ok(content) => println!("{content}"),
                Err(error) => eprintln!("{error}"),
            }
        }

        /// Set the widget content to the html
        pub fn with_content_html(self, html: String) -> Self {
            Self {
                widget_content: WidgetContent::Html(html),
                ..self
            }
        }

        /// Set the widget content to the text
        pub fn with_content_text(self, text: String) -> Self {
            Self {
                widget_content: WidgetContent::Text(text),
                ..self
            }
        }

        /// Set the widget to have an empty body
        pub fn with_content_empty(self, text: String) -> Self {
            Self {
                widget_content: WidgetContent::Text(text),
                ..self
            }
        }

        /// Signal the runtime to wait this duration in-between reruns of this widget
        pub fn rerun_in(self, rerun_in: Duration) -> Self {
            Self {
                rerun_in_ms: Some(rerun_in.as_millis()),
                ..self
            }
        }

        /// Get the rerun time as a duration
        pub fn rerun_duration(&self) -> Option<Duration> {
            self.rerun_in_ms
                .map(|duration| Duration::from_millis(duration as u64))
        }
    }
}
