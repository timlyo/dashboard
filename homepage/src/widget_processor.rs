use crate::process::ProcessResult;
use serde_with::{serde_as, DurationMilliSeconds, TimestampSecondsWithFrac};
use std::collections::HashMap;
use std::time::{Duration, Instant, SystemTime};

pub struct RenderResult {
    /// Mapping from widget_id to widget_html
    pub widgets: HashMap<String, RenderOutput>,
}

#[serde_as]
#[derive(serde::Serialize, Debug)]
pub struct RenderOutput {
    content: String,
    #[serde_as(as = "TimestampSecondsWithFrac")]
    time: SystemTime,
    #[serde_as(as = "DurationMilliSeconds")]
    duration: Duration,
}

pub async fn render_widget_updates(updates: Vec<(i64, Instant, ProcessResult)>) -> RenderResult {
    if updates.is_empty() {
        tracing::warn!(message = "Render was passed an empty updates list",);
    }

    let widgets = updates
        .into_iter()
        .filter_map(|(id, _time, result)| {
            process_result(result).map(|r| (format!("widget-{id}"), r))
        })
        .collect();

    RenderResult { widgets }
}

/// Form the output of the program into something to show the user on the screen
fn process_result(result: ProcessResult) -> Option<RenderOutput> {
    // TODO handle actions from output

    match result {
        ProcessResult {
            structured_output: Some(structured_output),
            time,
            duration,
            ..
        } => Some(RenderOutput {
            content: structured_output.widget_content().unwrap_or("").to_string(),
            time,
            duration,
        }),

        ProcessResult {
            structured_output: None,
            time,
            duration,
            stdout,
            ..
        } => {
            // Assume HTML if starts with `<`. I should probably be smarter than this
            let content = if stdout.starts_with('<') {
                stdout
            } else {
                // Assume plain text output
                stdout.replace('\n', "<br>")
            };

            Some(RenderOutput {
                content,
                time,
                duration,
            })
        }
    }
}
