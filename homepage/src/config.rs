use crate::error::Error;
use figment::providers::{Env, Format, Toml};
use figment::Figment;
use std::net::IpAddr;

#[derive(serde::Deserialize)]
pub struct Config {
    #[serde(default = "default_port")]
    pub port: u16,
    #[serde(default = "default_host")]
    pub host: IpAddr,
}

fn default_port() -> u16 {
    8921
}

fn default_host() -> IpAddr {
    "0.0.0.0".parse().unwrap()
}

pub fn load_config() -> Result<Config, Error> {
    Figment::new()
        .merge(Toml::file("config.toml"))
        .merge(Env::prefixed("DASHBOARD_"))
        .extract()
        .map_err(Error::from)
}
