use crate::forms::{NewProfile, Widget};
use crate::models::{Command, Response};
use crate::scheduler::SchedulerRegistry;
use crate::templates::Index;
use crate::widget_processor::render_widget_updates;

use crate::output_registry::OutputRegistry;
use axum::extract::Extension;
use axum::Router;
use axum_typed_websockets::{Message, WebSocket};
use futures::{SinkExt, StreamExt};
use std::net::SocketAddr;
use tower::ServiceBuilder;
use tower_http::compression::CompressionLayer;
use tower_http::trace::{DefaultMakeSpan, DefaultOnRequest, TraceLayer};
use tracing::Level;
use tracing_subscriber::layer::SubscriberExt;
use tracing_subscriber::util::SubscriberInitExt;
use tracing_subscriber::Layer;

mod config;
mod database;
mod error;
mod forms;
mod models;
mod output_registry;
mod process;
mod routes;
mod scheduler;
mod templates;
mod widget_processor;

#[tracing::instrument(skip(socket, output_registry))]
async fn handle_socket(
    socket: WebSocket<Response, Command>,
    profile: u32,
    output_registry: OutputRegistry,
) {
    let (mut sink, mut stream) = socket.split();

    tokio::spawn(async move {
        while let Some(msg) = stream.next().await {
            let msg = match msg {
                Ok(msg) => msg,
                Err(ref error) => {
                    tracing::error!(message = "Error decoding socket msg", %error);
                    continue;
                }
            };

            tracing::info!(message = "Got message from websocket", ?msg)
        }
    });

    tokio::spawn(async move {
        let mut last_check = std::time::Instant::now();

        // Send all existing ones
        let updates = output_registry.get_all_last_outputs(profile).await;
        let render_result = render_widget_updates(updates).await;
        let _ = sink
            .send(Message::Item(Response {
                widgets: render_result.widgets,
                error: None,
            }))
            .await;

        loop {
            let updates = output_registry.get_outputs_since(profile, last_check).await;

            if !updates.is_empty() {
                let render_result = render_widget_updates(updates).await;
                let result = sink
                    .send(Message::Item(Response {
                        widgets: render_result.widgets,
                        error: None,
                    }))
                    .await;

                if let Err(error) = result {
                    tracing::error!(message = "Failed to send new html. Closing socket", ?error);
                    return sink.close().await;
                }
            }

            last_check = std::time::Instant::now();

            tokio::time::sleep(tokio::time::Duration::from_millis(50)).await;
        }
    });
}

#[tokio::main]
async fn main() {
    // let console_layer = console_subscriber::spawn();

    let config = config::load_config().expect("Failed to load config");

    tracing_subscriber::registry()
        // .with(console_layer)
        .with(
            tracing_subscriber::fmt::layer()
                .compact()
                .with_filter(tracing_subscriber::filter::EnvFilter::from_default_env()),
        )
        .init();

    let db = database::get_db().await.unwrap();

    sqlx::migrate!("../migrations")
        .run(&db)
        .await
        .expect("Failed to migrate database");

    let output_registry = OutputRegistry::new();

    let scheduler = SchedulerRegistry::new(db.clone(), output_registry.clone());

    let service = ServiceBuilder::new().layer(
        TraceLayer::new_for_http()
            .make_span_with(DefaultMakeSpan::new().level(Level::INFO))
            .on_request(DefaultOnRequest::new().level(Level::INFO)),
    );

    let app = Router::new()
        .merge(routes::router())
        .layer(service)
        .layer(CompressionLayer::new())
        .layer(Extension(db))
        .layer(Extension(scheduler))
        .layer(Extension(output_registry));

    let address = SocketAddr::new(config.host, config.port);

    tracing::info!(message = "Listening", %address);

    axum::Server::bind(&address)
        .serve(app.into_make_service())
        .await
        .unwrap();
}
