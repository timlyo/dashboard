use cynic::impl_scalar;

mod schema {
    cynic::use_schema!("schema.graphql");
}

#[derive(cynic::QueryFragment, Debug, serde::Deserialize)]
#[cynic(schema_path = "schema.graphql", argument_struct = "Arguments")]
#[serde(rename_all = "camelCase")]
pub struct Query {
    #[arguments(owner = &args.owner, name = &args.name)]
    pub repository: Option<Repository>,
}

#[derive(cynic::FragmentArguments, Debug)]
pub struct Arguments {
    pub owner: String,
    pub name: String,
    pub pr_states: Option<Vec<PullRequestState>>,
}

#[derive(cynic::QueryFragment, Debug, serde::Deserialize)]
#[cynic(schema_path = "schema.graphql", argument_struct = "Arguments")]
#[serde(rename_all = "camelCase")]
pub struct Repository {
    pub name: String,
    pub owner: RepositoryOwner,
    #[arguments(last = 10, states = &args.pr_states)] // TODO filter by state
    pub pull_requests: PullRequestConnection,
    // TODO deployments, environments, forks, issues, milestones, is_fork, primary_language, latestRelease
}

#[derive(cynic::QueryFragment, Debug, serde::Deserialize)]
#[cynic(schema_path = "schema.graphql", argument_struct = "Arguments")]
#[serde(rename_all = "camelCase")]
pub struct RepositoryOwner {
    pub login: String,
}

#[derive(cynic::QueryFragment, Debug, serde::Deserialize)]
#[cynic(schema_path = "schema.graphql")]
#[serde(rename_all = "camelCase")]
pub struct PullRequestConnection {
    pub nodes: Option<Vec<Option<PullRequest>>>,
}

#[derive(cynic::QueryFragment, Debug, serde::Deserialize)]
#[cynic(schema_path = "schema.graphql")]
#[serde(rename_all = "camelCase")]
pub struct PullRequest {
    pub created_at: chrono::DateTime<chrono::Utc>,
    pub additions: i32,
    pub author: Option<Actor>,
    pub changed_files: i32,
    #[arguments(last = 10)]
    pub commits: PullRequestCommitConnection,
    pub closed: bool,
    pub checks_url: String,
    pub is_draft: bool,
    pub merged: bool,
    pub merge_commit: Option<Commit>,
    pub milestone: Option<Milestone>,
    /// Id of the pull request
    pub number: i32,
    pub permalink: String,
    pub title: String,
    pub url: String,
    // TODO assignees, approved, reactions, reviews, state, mergeable
}

impl PullRequest {
    pub fn get_check_status(&self) -> Option<StatusState> {
        if let Some(commits) = &self.commits.nodes {
            commits
                .iter()
                .rev()
                .filter_map(|c| c.as_ref())
                .next()
                .and_then(|c| c.commit.status_check_rollup.as_ref())
                .map(|s| s.state)
        } else {
            None
        }
    }

    pub fn get_check_status_as_icon(&self) -> String {
        match self.get_check_status() {
            Some(StatusState::Expected) => {
                String::from(r#"<i class="fa-solid fa-circle orange"></i>"#)
            }
            Some(StatusState::Success) => {
                String::from(r#"<i class="fa-solid fa-check green"></i>"#)
            }
            Some(StatusState::Error | StatusState::Failure) => {
                String::from(r#"<i class="fa-solid fa-x red"></i>"#)
            }
            Some(StatusState::Pending) => {
                String::from(r#"<i class="fa-solid fa-hourglass yellow"></i>"#)
            }
            other => format!("<!-- Unknown status: {other:?} -->"),
        }
    }
}

#[derive(cynic::Enum, Clone, Copy, Debug, serde::Deserialize, Eq, PartialEq)]
#[cynic(schema_path = "schema.graphql")]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum PullRequestState {
    Open,
    Closed,
    Merged,
}

#[derive(cynic::QueryFragment, Debug, serde::Deserialize)]
#[cynic(schema_path = "schema.graphql")]
#[serde(rename_all = "camelCase")]
pub struct PullRequestCommitConnection {
    pub total_count: i32,
    pub nodes: Option<Vec<Option<PullRequestCommit>>>,
}

#[derive(cynic::QueryFragment, Debug, serde::Deserialize)]
#[cynic(schema_path = "schema.graphql")]
#[serde(rename_all = "camelCase")]
pub struct PullRequestCommit {
    commit: Commit,
}

#[derive(cynic::QueryFragment, Debug, serde::Deserialize)]
#[cynic(schema_path = "schema.graphql")]
#[serde(rename_all = "camelCase")]
pub struct Actor {
    pub avatar_url: String,
    pub login: String,
    pub url: String,
}

#[derive(cynic::QueryFragment, Debug, serde::Deserialize)]
#[cynic(schema_path = "schema.graphql")]
#[serde(rename_all = "camelCase")]
pub struct Commit {
    pub id: cynic::Id,
    pub additions: i32,
    pub deletions: i32,
    pub status_check_rollup: Option<StatusCheckRollup>,
}

#[derive(cynic::QueryFragment, Debug, serde::Deserialize)]
#[cynic(schema_path = "schema.graphql")]
#[serde(rename_all = "camelCase")]
pub struct StatusCheckRollup {
    pub state: StatusState,
}

#[derive(cynic::Enum, Clone, Copy, Debug, serde::Deserialize, Eq, PartialEq)]
#[cynic(schema_path = "schema.graphql")]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum StatusState {
    Error,
    Expected,
    Failure,
    Pending,
    Success,
}

#[derive(cynic::QueryFragment, Debug, serde::Deserialize)]
#[cynic(schema_path = "schema.graphql")]
#[serde(rename_all = "camelCase")]
pub struct CheckSuiteConnection {
    pub total_count: i32,
    pub nodes: Option<Vec<Option<CheckSuite>>>,
}

#[derive(cynic::QueryFragment, Debug, serde::Deserialize)]
#[cynic(schema_path = "schema.graphql")]
#[serde(rename_all = "camelCase")]
pub struct CheckSuite {
    pub conclusion: Option<CheckConclusionState>,
}

#[derive(cynic::Enum, Clone, Copy, Debug, serde::Deserialize, Eq, PartialEq)]
#[cynic(schema_path = "schema.graphql")]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum CheckConclusionState {
    ActionRequired,
    Cancelled,
    Failure,
    Neutral,
    Skipped,
    Stale,
    StartupFailure,
    Success,
    TimedOut,
}

#[derive(cynic::QueryFragment, Debug, serde::Deserialize)]
#[cynic(schema_path = "schema.graphql")]
#[serde(rename_all = "camelCase")]
pub struct Status {
    pub state: StatusState,
}

#[derive(cynic::QueryFragment, Debug, serde::Deserialize)]
#[cynic(schema_path = "schema.graphql")]
#[serde(rename_all = "camelCase")]
pub struct Milestone {
    pub closed: bool,
    pub created_at: chrono::DateTime<chrono::Utc>,
    pub creator: Option<Actor>,
    pub description: Option<String>,
    pub number: i32,
    pub progress_percentage: f64,
    pub title: String,
    pub due_on: Option<chrono::DateTime<chrono::Utc>>,
    pub url: String,
    // TODO issues, state
}

type DateTime = chrono::DateTime<chrono::Utc>;
impl_scalar!(DateTime, schema::DateTime);

type Uri = String;
impl_scalar!(Uri, schema::Uri);
