mod queries;
use crate::queries::{PullRequest, PullRequestState, Query, Repository};
use askama::Template;
use cynic::{serde_json, QueryBuilder};
use itertools::Itertools;

#[derive(serde::Deserialize)]
struct State {
    name: String,
    owner: String,
    token: String,
}

#[tokio::main]
async fn main() {
    let args = homepage::parse_stdin().unwrap();
    let state: State = args.get_state().unwrap();

    let query = queries::Query::build(queries::Arguments {
        name: state.name,
        owner: state.owner,
        pr_states: Some(vec![PullRequestState::Open]),
    });

    let client = reqwest::Client::new();

    let response = client
        .post("https://api.github.com/graphql")
        .header("User-Agent", "cynic")
        .bearer_auth(state.token.as_str())
        .json(&query)
        .send()
        .await
        .unwrap();

    let data = response.json::<serde_json::Value>().await.unwrap();

    if let Some(data) = data.get("data") {
        let query: Query = serde_json::from_value(data.clone()).unwrap();

        if let Some(repository) = query.repository {
            let rendered = render_repo(repository);
            println!("{rendered}");
        }
    } else if let Some(error) = data.get("errors") {
        println!("{}", serde_json::to_string_pretty(&error).unwrap());
        std::process::exit(1);
    }
}

#[derive(Template)]
#[template(path = "template.html")]
pub struct RepoTemplate {
    owner: String,
    name: String,
    pull_requests: Vec<PullRequest>,
}

fn render_repo(repo: Repository) -> String {
    let pull_requests = if let Some(prs) = repo.pull_requests.nodes {
        prs.into_iter().flatten().collect_vec()
    } else {
        Vec::new()
    };

    let template = RepoTemplate {
        owner: repo.owner.login,
        name: repo.name,
        pull_requests,
    };

    template.render().unwrap()
}
