use askama::Template;

#[derive(serde::Deserialize)]
struct State {
    address: String,
}

#[allow(dead_code)]
#[derive(serde::Deserialize)]
struct Status {
    domains_being_blocked: u32,
    dns_queries_today: u32,
    ads_blocked_today: u32,
    ads_percentage_today: f32,
    unique_domains: u32,
    queries_forwarded: u32,
    queries_cached: u32,
    clients_ever_seen: u32,
    unique_clients: u32,
    dns_queries_all_types: u32,
    #[serde(rename = "reply_UNKNOWN")]
    reply_unknown: u32,
    #[serde(rename = "reply_NODATA")]
    reply_nodata: u32,
    #[serde(rename = "reply_NXDOMAIN")]
    reply_nxdomain: u32,
    #[serde(rename = "reply_CNAME")]
    reply_cname: u32,
    #[serde(rename = "reply_IP")]
    reply_ip: u32,
    #[serde(rename = "reply_DOMAIN")]
    reply_domain: u32,
    #[serde(rename = "reply_RRNAME")]
    reply_rrname: u32,
    #[serde(rename = "reply_SERVFAIL")]
    reply_servfail: u32,
    #[serde(rename = "reply_REFUSED")]
    reply_refused: u32,
    #[serde(rename = "reply_NOTIMP")]
    reply_notimp: u32,
    #[serde(rename = "reply_OTHER")]
    reply_other: u32,
    #[serde(rename = "reply_DNSSEC")]
    reply_dnssec: u32,
    #[serde(rename = "reply_NONE")]
    reply_none: u32,
    #[serde(rename = "reply_BLOB")]
    reply_blob: u32,
    dns_queries_all_replies: u32,
    privacy_level: u32,
    status: String,
    gravity_last_updated: Gravity,
}

#[allow(dead_code)]
#[derive(serde::Deserialize)]
struct Gravity {
    file_exists: bool,
    absolute: u64,
    relative: RelativeTime,
}

#[allow(dead_code)]
#[derive(serde::Deserialize)]
struct RelativeTime {
    days: u32,
    hours: u32,
    minutes: u32,
}

#[tokio::main]
async fn main() {
    let args = homepage::parse_stdin().unwrap();
    let state = args.get_state::<State>().unwrap();

    let status = get_status(&state.address).await;

    match status {
        Ok(status) => {
            let output = PageTemplate {
                address: state.address,
                status,
            }
            .render()
            .unwrap();

            println!("{output}");
        }
        Err(e) => println!("Pihole error: {}", e),
    }
}

async fn get_status(address: &str) -> Result<Status, reqwest::Error> {
    let client = reqwest::Client::new();

    let address = if address.contains("/admin/api.php") {
        address.to_string()
    } else {
        format!("{address}/admin/api.php")
    };

    dbg!(&address);

    let response = client.get(address).send().await?;

    response.json().await
}

#[derive(Template)]
#[template(path = "template.html")]
struct PageTemplate {
    address: String,
    status: Status,
}
