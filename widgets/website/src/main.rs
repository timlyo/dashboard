use askama::Template;
use homepage::StructuredOutput;
use itertools::Itertools;
use scraper::Selector;
use std::time::{Duration, Instant};

#[derive(serde::Deserialize)]
struct State {
    url: String,
    custom_title: Option<String>,
}

#[tokio::main]
async fn main() {
    let args = homepage::parse_stdin().unwrap();
    let state = args.get_state::<State>().unwrap();

    let client = reqwest::Client::new();

    let start = Instant::now();
    let response = client.get(&state.url).send().await.expect("Request Failed");
    let response_time = Instant::now() - start;

    let status = response.status();

    let page_data = dbg!(response.text().await.ok().map(|page| get_page_data(&page)));

    let title = match (state.custom_title.as_deref(), &page_data) {
        (Some(custom), _) => custom,
        (
            None,
            Some(PageData {
                title: Some(title), ..
            }),
        ) => title.as_str(),
        _ => state.url.as_str(),
    };

    let icon_url = page_data
        .as_ref()
        .and_then(|pd| pd.favicon.clone())
        .unwrap_or(format!("{url}/favicon.ico", url = state.url));

    let response = PageTemplate {
        status,
        response_time,
        url: state.url.as_str(),
        title,
        icon_url,
    }
    .render()
    .unwrap();

    StructuredOutput::new()
        .with_content_html(response)
        .rerun_in(Duration::from_secs(60 * 5))
        .print()
}

#[derive(Debug)]
struct PageData {
    title: Option<String>,
    favicon: Option<String>,
}

fn get_page_data(page: &str) -> PageData {
    let document = scraper::Html::parse_document(page);

    let title = document
        .select(&Selector::parse("title").unwrap())
        .next()
        .map(|e| e.text().join(""));

    let favicon = document
        .select(&Selector::parse(r#"link[rel="icon"]"#).unwrap())
        .next()
        .map(|e| e.text().join(""));

    PageData { title, favicon }
}

#[derive(Template)]
#[template(path = "template.html")]
struct PageTemplate<'a> {
    status: reqwest::StatusCode,
    title: &'a str,
    url: &'a str,
    response_time: Duration,
    icon_url: String,
}

impl<'a> PageTemplate<'a> {
    pub fn format_response_time(&self) -> String {
        let time = self.response_time.as_millis();

        format!("{time}ms")
    }

    pub fn format_status(&self) -> &'static str {
        if self.status.is_success() {
            r#"<i class="fa-solid fa-check green"></i>"#
        } else {
            r#"<i class="fa-solid fa-x red"></i>"#
        }
    }
}
