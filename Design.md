- List of shortcuts
- Runnable commands to get more data about those shortcuts
  - Commands are just a binary that is passed arguments and returns JSON
  - Must have a way of whitelisting commands so this is a remote code execution platform
- Timeseries DB to store data returned from commands
- Front end can send requests at variable rates
- Programs take in state and events, and then return HTML
- List of widgets is loaded each time

# Profiles and tags

- All widgets can have a tag
- Tags can be grouped into profiles

- e.g. profiles could be Laura and Tim, tags could be:
  - Tim, work
  - Tim, personal
  - Laura, work
  - Laura, personal
  - house
  - bills
- Tim profile could then load all Laura, work, personal, house, and bills and group them in the interface by tag
